(* ----------------------------------------- *)
(*           SWISS KNIFE PROTOCOL            *)
(*     Distance Hijacking Fraud Analysis     *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a key K and a one-way keyed-function H, there is a system-wide constant cS.
V -> P : nV
P -> V : nP
V -> P : cV
P -> V : Answer(cV,H(<cS,nP>,K),XOR(R0,K))
P -> V : H(<idP,nV,nP,challenges>,K)
V -> P : H(nP,K)
*)

(* -- Signature --- *)
const OK:bitstring.

(* One-Way keyed-function *)
fun H(bitstring,bitstring):bitstring.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Shared key *)
fun sharedKey(bitstring,bitstring):bitstring [private].

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring):bitstring.

(* Pair function *)
(* NOTE : We need a special pairing function for our analysis of distance hijacking fraud using our modified proverif executable.*)
fun Pair(bitstring,bitstring):bitstring.
fun Proj1(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj1(Pair(x,y)) = x.
fun Proj2(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj2(Pair(x,y)) = y.

(* --- Constants / Channels --- *)
free cP:channel.
free cS:bitstring.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Dishonest Player far-away from idV0 *)
free idE0:bitstring. (* Honest Player far-away from idV0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in
	new cV:bitstring;	
	phase 1;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf);
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	
	
let Verifier_P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	

let Verifier_P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 2;	
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).		
	
let Verifier_P0P0P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 1;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	

let Verifier_P0P0P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	
	
let Verifier_P0P1P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P0P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P0P2P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P1P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P1P2P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H(Pair(cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H(Pair(cV,Pair(ipID,Pair(nV,inV1))),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P2(pID:bitstring,ivID:bitstring) =
	phase 2;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P0P1(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 1;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P0P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P1P1(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P1P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P2P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.	
	
let Prover_P1P1P2(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P1P2P2(pID:bitstring,ivID:bitstring) =	
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H(Pair(cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H(Pair(inP2,Pair(pID,Pair(inP1,nP))),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,sharedKey(idV0,idP0));
	out(cP,sharedKey(idP0,idV0));
	out(cP,sharedKey(idP0,idP0));
	out(cP,sharedKey(idP0,idE0));
	out(cP,sharedKey(idE0,idP0));
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the DHR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |	
	!Verifier_P0(idV0,idE0) | !Verifier_P1(idV0,idE0) | !Verifier_P2(idV0,idE0) |
	!Verifier_P0P0P1(idV0,idE0) | !Verifier_P0P0P2(idV0,idE0) | !Verifier_P0P1P1(idV0,idE0) |	
	!Verifier_P0P1P2(idV0,idE0) | !Verifier_P0P2P2(idV0,idE0) | 
	!Verifier_P1P1P2(idV0,idE0) | !Verifier_P1P2P2(idV0,idE0) |		
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P0P1(idV0,idV0) | !Prover_P0P0P2(idV0,idV0) | !Prover_P0P1P1(idV0,idV0) |	
	!Prover_P0P1P2(idV0,idV0) | !Prover_P0P2P2(idV0,idV0) | 
	!Prover_P1P1P2(idV0,idV0) | !Prover_P1P2P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P0P1(idV0,idP0) | !Prover_P0P0P2(idV0,idP0) | !Prover_P0P1P1(idV0,idP0) |	
	!Prover_P0P1P2(idV0,idP0) | !Prover_P0P2P2(idV0,idP0) | 
	!Prover_P1P1P2(idV0,idP0) | !Prover_P1P2P2(idV0,idP0) |	
	!Prover_P0(idV0,idE0) | !Prover_P1(idV0,idE0) | !Prover_P2(idV0,idE0) |
	!Prover_P0P0P1(idV0,idE0) | !Prover_P0P0P2(idV0,idE0) | !Prover_P0P1P1(idV0,idE0) |	
	!Prover_P0P1P2(idV0,idE0) | !Prover_P0P2P2(idV0,idE0) | 
	!Prover_P1P1P2(idV0,idE0) | !Prover_P1P2P2(idV0,idE0) |	
	(* idE0 only speaks in P0/P2 *)
	!Verifier_P0(idE0,idV0) | !Verifier_P2(idE0,idV0) | !Verifier_P0P0P2(idE0,idV0) | !Verifier_P0P2P2(idE0,idV0) |
	!Verifier_P0(idE0,idP0) | !Verifier_P2(idE0,idP0) | !Verifier_P0P0P2(idE0,idP0) | !Verifier_P0P2P2(idE0,idP0) |
	!Verifier_P0(idE0,idE0) | !Verifier_P2(idE0,idE0) | !Verifier_P0P0P2(idE0,idE0) | !Verifier_P0P2P2(idE0,idE0) |	
	!Prover_P0(idE0,idV0) | !Prover_P2(idE0,idV0) | !Prover_P0P0P2(idE0,idV0) | !Prover_P0P2P2(idE0,idV0) |
	!Prover_P0(idE0,idP0) | !Prover_P2(idE0,idP0) | !Prover_P0P0P2(idE0,idP0) | !Prover_P0P2P2(idE0,idP0) |
	!Prover_P0(idE0,idE0) | !Prover_P2(idE0,idE0) | !Prover_P0P0P2(idE0,idE0) | !Prover_P0P2P2(idE0,idE0)