(* ----------------------------------------- *)
(*               CRCS PROTOCOL               *)
(*          No-revealing Signature           *)
(*     Distance Hijacking Fraud Analysis     *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P has a couple of public/secret key for signature purposes.
P -> V : sign(commit(nP),sk(idP))
V -> P : cV
P -> V :(cV,nP)
P -> V : (idV,nP,cV),sign((idV,nP,cV),sk(idP))
*)

(* -- Signature --- *)
const OK:bitstring.

(* Commitment *)
fun Commit(bitstring):bitstring.

(* Signature *)
fun pubKey(bitstring):bitstring.
fun secKey(bitstring):bitstring [private].
fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring,bitstring):bitstring
reduc forall m:bitstring, k:bitstring; Verify(m,Sign(m,secKey(k)),pubKey(k)) = OK.

(* Equality test *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Pair function *)
(* NOTE : We need a special pairing function for our analysis of distance hijacking fraud using our modified proverif executable.*)
fun Pair(bitstring,bitstring):bitstring.
fun Proj1(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj1(Pair(x,y)) = x.
fun Proj2(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj2(Pair(x,y)) = y.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Dishonest Player far-away from idV0 *)
free idE0:bitstring. (* Honest Player far-away from idV0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	phase 1;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	phase 2;
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	event EndV(vID,pID).
	
let Verifier_P0(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.

let Verifier_P2(vID:bitstring,pID:bitstring) =
	phase 2;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Verifier_P0P0P1(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	phase 1;
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Verifier_P0P0P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	phase 2;
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.

let Verifier_P0P1P1(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.

let Verifier_P0P1P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	phase 2;
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.

let Verifier_P0P2P2(vID:bitstring,pID:bitstring) =
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1P1P2(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	phase 2;
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Verifier_P1P2P2(vID:bitstring,pID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let checkCV = equals(cV,Proj1(inV2)) in
	let inNP = Proj2(inV2) in
	in(cP,inV3:bitstring);
	let inMP = Proj1(inV3) in
	let inSP = Proj2(inV3) in
	let TestSign = Verify(inMP,inSP,pubKey(pID)) in
	let checkMess = equals(inMP,Pair(vID,Pair(inNP,cV))) in
	let TestComm = Verify(Commit(inNP),inV1,pubKey(pID)) in
	0.
	
let Prover_P0(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	in(cP,inP1:bitstring);
	out(cP,Pair(inP1,nP));
	out(cP,Pair(Pair(vID,Pair(nP,inP1)),Sign(Pair(vID,Pair(nP,inP1)),secKey(pID)))).

let Prover_P1(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	phase 1;
	in(cP,inP1:bitstring);
	out(cP,Pair(inP1,nP));
	out(cP,Pair(Pair(vID,Pair(nP,inP1)),Sign(Pair(vID,Pair(nP,inP1)),secKey(pID)))).

let Prover_P2(pID:bitstring,vID:bitstring) =
	new nP:bitstring;
	out(cP,Sign(Commit(nP),secKey(pID)));
	phase 2;
	in(cP,inP1:bitstring);
	out(cP,Pair(inP1,nP));
	out(cP,Pair(Pair(vID,Pair(nP,inP1)),Sign(Pair(vID,Pair(nP,inP1)),secKey(pID)))).	

(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,secKey(idP0));
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the DHR property *)
	Verifier_Test(idV0,idP0) |
	(* -- Processes that can also speak during the execution : -- *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |
	!Verifier_P0(idV0,idE0) | !Verifier_P1(idV0,idE0) | !Verifier_P2(idV0,idE0) |
	!Verifier_P0P0P1(idV0,idE0) | !Verifier_P0P0P2(idV0,idE0) | !Verifier_P0P1P1(idV0,idE0) |	
	!Verifier_P0P1P2(idV0,idE0) | !Verifier_P0P2P2(idV0,idE0) | 
	!Verifier_P1P1P2(idV0,idE0) | !Verifier_P1P2P2(idV0,idE0) |	
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |	
	!Prover_P0(idV0,idE0) | !Prover_P1(idV0,idE0) | !Prover_P2(idV0,idE0) |
	(* idE0 only speaks in P0/P2 *)
	!Verifier_P0(idE0,idV0) | !Verifier_P2(idE0,idV0) | !Verifier_P0P0P2(idE0,idV0) | !Verifier_P0P2P2(idE0,idV0) |
	!Verifier_P0(idE0,idP0) | !Verifier_P2(idE0,idP0) | !Verifier_P0P0P2(idE0,idP0) | !Verifier_P0P2P2(idE0,idP0) |
	!Verifier_P0(idE0,idE0) | !Verifier_P2(idE0,idE0) | !Verifier_P0P0P2(idE0,idE0) | !Verifier_P0P2P2(idE0,idE0) |	
	!Prover_P0(idE0,idV0) | !Prover_P2(idE0,idV0) | 
	!Prover_P0(idE0,idP0) | !Prover_P2(idE0,idP0) | 
	!Prover_P0(idE0,idE0) | !Prover_P2(idE0,idE0) 