(* ----------------------------------------- *)
(*             PAYSAFE PROTOCOL              *)
(*     Distance Hijacking Fraud Analysis     *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* 
R and C exchange public constants 
--- Begin Rapid Phase ---
R -> C : PConstant, Amount, rNonce 
C -> R : cNonce, PConstants
--- End Rapid Phase ---
C provides info to authenticate itself to R
*)

(* -- Signature --- *)
const OK:bitstring.
	
(* Public Key Signature *)
fun pubKey(bitstring):bitstring.
fun secKey(bitstring):bitstring [private].
fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring):bitstring
reduc forall m:bitstring, k:bitstring; Verify(Sign(m,secKey(k)),pubKey(k)) = m.
	
(* Mac *)
fun genKey(bitstring,bitstring):bitstring.
fun Mac(bitstring,bitstring):bitstring.
	
(* Equality test *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Pair function *)
(* NOTE : We need a special pairing function for our analysis of distance hijacking fraud using our modified proverif executable.*)
fun Pair(bitstring,bitstring):bitstring.
fun Proj1(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj1(Pair(x,y)) = x.
fun Proj2(bitstring):bitstring
reduc forall x:bitstring, y:bitstring; Proj2(Pair(x,y)) = y.

(* Card function *)
fun Atc(bitstring):bitstring [private].
fun TestATC(bitstring):bitstring
reduc forall id:bitstring; TestATC(Atc(id)) = id.
fun Pan(bitstring):bitstring [private].
fun TestPAN(bitstring):bitstring
reduc forall id:bitstring; TestPAN(Pan(id)) = id.
fun expDate(bitstring):bitstring [private].
fun TestEXPDATE(bitstring):bitstring
reduc forall id:bitstring; TestEXPDATE(expDate(id)) = id.

(* --- Constants / Channels --- *)
free cP:channel.
free idR0:bitstring. (* Honest Player acting as Test Reader *)
free idC0:bitstring. (* Dishonest Player far-away from idR0 *)
free idE0:bitstring. (* Honest Player far-away from idR0 *)
free idBank:bitstring.

free AID:bitstring.
free GENERATE_AC:bitstring.
free GPO:bitstring.
free PAYSYSDDF:bitstring.
free PDOL:bitstring.
free READ_RECORD:bitstring.
free SELECT:bitstring.

(* --- Events --- *)
event EndR(bitstring,bitstring).

(* NB: We removed the 2 initials in/out since they are not really relevant for the security of the protocol. *)

(* --- Processes --- *)
let Reader_Test(rID:bitstring,icID:bitstring) =
	new amount:bitstring;
	(* We output the amount to give it to the attacker since it's not a secret *)	
	out(cP,amount);
(*	out(cP,Pair(SELECT,PAYSYSDDF));        *)
(*	in(cP,cAID:bitstring);            	   *)
(*	out(cP,Pair(SELECT,cAID));             *)
(*	in(cP,=PDOL);             			   *)
	new rNonce:bitstring;
	phase 1;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);
	phase 2;	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in
	event EndR(rID,icID).	
	
let Reader_P0(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P1(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	phase 1;
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.

let Reader_P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	phase 2;
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P0P0P1(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	phase 1;
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P0P0P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P0P1P1(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	phase 1;
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P0P1P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	phase 1;
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P0P2P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P1P1P1(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	phase 1;
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P1P1P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	phase 1;
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P1P2P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	phase 1;
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P0P2P2P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	phase 2;
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P1P1P1P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	phase 1;
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P1P1P2P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	phase 1;
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	phase 2;
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.
	
let Reader_P1P2P2P2(rID:bitstring,icID:bitstring) =
	new amount:bitstring;	
	out(cP,amount);
	new rNonce:bitstring;
	out(cP,Pair(GPO,Pair(amount,rNonce)));
	phase 1;
	in(cP,inR1:bitstring);	
	let cNonce = Proj1(inR1) in
	let cATC = Proj1(Proj2(inR1)) in
	let CPAN = Proj2(Proj2(inR1)) in
	out(cP,GENERATE_AC);
	phase 2;
	in(cP,inR2:bitstring);
	let cSDAD = Proj1(inR2) in
	let cAC = Proj2(inR2) in
	out(cP,READ_RECORD);
	in(cP,cSSAD:bitstring);
	out(cP,READ_RECORD);
	in(cP,cCert:bitstring);	
	let cID = Proj1(Verify(cCert,pubKey(idBank))) in
	let cPubK = Proj2(Verify(cCert,pubKey(idBank))) in
	let inSDAD = Verify(cSDAD,cPubK) in
	let checkSDAD = equals(inSDAD,Pair(rNonce,Pair(cNonce,Pair(amount,Pair(cATC,cAC))))) in	
	0.	
	
let Card_P0(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
(*	in(cP,inC1:bitstring);    *)
(*	let TestA1 = equals(Proj1(inC1),SELECT) in      *)
(*	let TestA2 = equals(Proj2(inC1),PAYSYSDDF) in   *)
(*	out(cP,AID);                           *)
(*	in(cP,inC2:bitstring);    *)
(*	let TestB1 = equals(Proj1(inC2),SELECT) in      *)
(*	let TestB2 = equals(Proj2(inC2),AID) in         *)
(*	out(cP,PDOL);                          *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).

let Card_P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 2;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P0P0P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	phase 1;
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P0P0P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	phase 2;
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P0P1P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	phase 1;
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P0P1P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	phase 1;
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	phase 2;
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P0P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	phase 2;
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P1P1P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	phase 1;
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P1P1P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	phase 1;
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	phase 2;
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P1P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	phase 1;
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	phase 2;
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P0P2P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	phase 2;
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P1P1P1P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	phase 2;
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P1P1P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	phase 2;
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).
	
let Card_P1P2P2P2(cID:bitstring,irID:bitstring) =   
	(* Card initialization  *)
    in(cP, PAN:bitstring);
	let Test1 = equals(cID, TestPAN(PAN)) in
 	in(cP, exp:bitstring);
	let Test2 = equals(cID, TestEXPDATE(exp)) in
	in(cP, Cert:bitstring);
	let Test3 = equals(pubKey(cID), Verify(Cert,pubKey(idBank))) in
	in(cP, SSAD:bitstring);
	let Test4 = equals((PAN,exp), Verify(SSAD,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let Test5 = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,inC3:bitstring);
	let TestC = equals(Proj1(inC3),GPO) in
	let rAMT = Proj1(Proj2(inC3)) in
	let rNonce = Proj2(Proj2(inC3)) in
	out(cP,Pair(cNonce,Pair(ATC,PAN)));
	phase 2;
	in(cP,inC4:bitstring);
	let TestD = equals(inC4,GENERATE_AC) in
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac(Pair(rAMT,Pair(rNonce,ATC)),macKey) in	
	let SDAD = Sign(Pair(rNonce,Pair(cNonce,Pair(rAMT,Pair(ATC,AC)))),secKey(cID)) in
	out(cP,Pair(SDAD,AC));
	in(cP,inC5:bitstring);
	let TestE = equals(inC5,READ_RECORD) in
	out(cP,SSAD);
	in(cP,inC6:bitstring);
	let TestF = equals(inC6,READ_RECORD) in
	out(cP,Cert).

(* --- Security Property --- *)
query event(EndR(idR0,idC0)).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,(idC0,secKey(idC0)));
	(* Outputting Cards Informations to initiate them *)
	out(cP,(Pan(idC0),Atc(idC0),expDate(idC0),Sign(Pair(idC0,pubKey(idC0)),secKey(idBank)),Sign((Pan(idC0),expDate(idC0)),secKey(idBank))));
	out(cP,(Pan(idR0),Atc(idR0),expDate(idR0),Sign(Pair(idR0,pubKey(idR0)),secKey(idBank)),Sign((Pan(idR0),expDate(idR0)),secKey(idBank))));
	out(cP,(Pan(idE0),Atc(idE0),expDate(idE0),Sign(Pair(idE0,pubKey(idE0)),secKey(idBank)),Sign((Pan(idE0),expDate(idE0)),secKey(idBank))));
	(* Now we let the adversary (almost) alone *)
	(* Honest Reader used to test the DHR property *)
	Reader_Test(idR0,idC0) |
	(* Processes that can also speak during the execution : *)	
	(* idR0 may execute Card/Reader roles at anytime *)	
	!Reader_P0(idR0,idC0) | !Reader_P1(idR0,idC0) | !Reader_P2(idR0,idC0) |
	!Reader_P0P0P0P1(idR0,idC0) | !Reader_P0P0P0P2(idR0,idC0) | !Reader_P0P0P1P1(idR0,idC0) |
	!Reader_P0P0P1P2(idR0,idC0) | !Reader_P0P0P2P2(idR0,idC0) | !Reader_P0P1P1P1(idR0,idC0) | 	
	!Reader_P0P1P1P2(idR0,idC0) | !Reader_P0P1P2P2(idR0,idC0) | !Reader_P0P2P2P2(idR0,idC0) |
	!Reader_P1P1P1P2(idR0,idC0) | !Reader_P1P1P2P2(idR0,idC0) | !Reader_P1P2P2P2(idR0,idC0) |
	!Reader_P0(idR0,idE0) | !Reader_P1(idR0,idE0) | !Reader_P2(idR0,idE0) |
	!Reader_P0P0P0P1(idR0,idE0) | !Reader_P0P0P0P2(idR0,idE0) | !Reader_P0P0P1P1(idR0,idE0) |
	!Reader_P0P0P1P2(idR0,idE0) | !Reader_P0P0P2P2(idR0,idE0) | !Reader_P0P1P1P1(idR0,idE0) | 	
	!Reader_P0P1P1P2(idR0,idE0) | !Reader_P0P1P2P2(idR0,idE0) | !Reader_P0P2P2P2(idR0,idE0) |
	!Reader_P1P1P1P2(idR0,idE0) | !Reader_P1P1P2P2(idR0,idE0) | !Reader_P1P2P2P2(idR0,idE0) |
	!Reader_P0(idR0,idR0) | !Reader_P1(idR0,idR0) | !Reader_P2(idR0,idR0) |
	!Reader_P0P0P0P1(idR0,idR0) | !Reader_P0P0P0P2(idR0,idR0) | !Reader_P0P0P1P1(idR0,idR0) |
	!Reader_P0P0P1P2(idR0,idR0) | !Reader_P0P0P2P2(idR0,idR0) | !Reader_P0P1P1P1(idR0,idR0) | 	
	!Reader_P0P1P1P2(idR0,idR0) | !Reader_P0P1P2P2(idR0,idR0) | !Reader_P0P2P2P2(idR0,idR0) |
	!Reader_P1P1P1P2(idR0,idR0) | !Reader_P1P1P2P2(idR0,idR0) | !Reader_P1P2P2P2(idR0,idR0) |	
	!Card_P0(idR0,idC0) | !Card_P1(idR0,idC0) | !Card_P2(idR0,idC0) |
	!Card_P0P0P0P1(idR0,idC0) | !Card_P0P0P0P2(idR0,idC0) | !Card_P0P0P1P1(idR0,idC0) |
	!Card_P0P0P1P2(idR0,idC0) | !Card_P0P0P2P2(idR0,idC0) | !Card_P0P1P1P1(idR0,idC0) | 	
	!Card_P0P1P1P2(idR0,idC0) | !Card_P0P1P2P2(idR0,idC0) | !Card_P0P2P2P2(idR0,idC0) |
	!Card_P1P1P1P2(idR0,idC0) | !Card_P1P1P2P2(idR0,idC0) | !Card_P1P2P2P2(idR0,idC0) |
	!Card_P0(idR0,idE0) | !Card_P1(idR0,idE0) | !Card_P2(idR0,idE0) |
	!Card_P0P0P0P1(idR0,idE0) | !Card_P0P0P0P2(idR0,idE0) | !Card_P0P0P1P1(idR0,idE0) |
	!Card_P0P0P1P2(idR0,idE0) | !Card_P0P0P2P2(idR0,idE0) | !Card_P0P1P1P1(idR0,idE0) | 	
	!Card_P0P1P1P2(idR0,idE0) | !Card_P0P1P2P2(idR0,idE0) | !Card_P0P2P2P2(idR0,idE0) |
	!Card_P1P1P1P2(idR0,idE0) | !Card_P1P1P2P2(idR0,idE0) | !Card_P1P2P2P2(idR0,idE0) |
	!Card_P0(idR0,idR0) | !Card_P1(idR0,idR0) | !Card_P2(idR0,idR0) |
	!Card_P0P0P0P1(idR0,idR0) | !Card_P0P0P0P2(idR0,idR0) | !Card_P0P0P1P1(idR0,idR0) |
	!Card_P0P0P1P2(idR0,idR0) | !Card_P0P0P2P2(idR0,idR0) | !Card_P0P1P1P1(idR0,idR0) | 	
	!Card_P0P1P1P2(idR0,idR0) | !Card_P0P1P2P2(idR0,idR0) | !Card_P0P2P2P2(idR0,idR0) |
	!Card_P1P1P1P2(idR0,idR0) | !Card_P1P1P2P2(idR0,idR0) | !Card_P1P2P2P2(idR0,idR0) |	
	(* idE0 only speaks in P0/P2 *)
	!Reader_P0(idE0,idR0) | !Reader_P2(idE0,idR0) |
	!Reader_P0P0P0P2(idE0,idR0) | !Reader_P0P0P2P2(idE0,idR0) | !Reader_P0P2P2P2(idE0,idR0) |
	!Reader_P0(idE0,idC0) | !Reader_P2(idE0,idC0) |
	!Reader_P0P0P0P2(idE0,idC0) | !Reader_P0P0P2P2(idE0,idC0) | !Reader_P0P2P2P2(idE0,idC0) |
	!Reader_P0(idE0,idE0) | !Reader_P2(idE0,idE0) |
	!Reader_P0P0P0P2(idE0,idE0) | !Reader_P0P0P2P2(idE0,idE0) | !Reader_P0P2P2P2(idE0,idE0) |	
	!Card_P0(idE0,idR0) | !Card_P0(idE0,idR0) |
	!Card_P0P0P0P2(idE0,idR0) | !Card_P0P0P2P2(idE0,idR0) | !Card_P0P2P2P2(idE0,idR0) |
	!Card_P0(idE0,idC0) | !Card_P0(idE0,idC0) |
	!Card_P0P0P0P2(idE0,idC0) | !Card_P0P0P2P2(idE0,idC0) | !Card_P0P2P2P2(idE0,idC0) |
	!Card_P0(idE0,idE0) | !Card_P0(idE0,idE0) |
	!Card_P0P0P0P2(idE0,idE0) | !Card_P0P0P2P2(idE0,idE0) | !Card_P0P2P2P2(idE0,idE0)
