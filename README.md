# So near and yet so far – Symbolic verification of distance-bounding protocols

## Abstract
The continuous adoption of Near Field Communication (NFC) tags offers many new application whose secu- rity is essential (e.g. contactless payments). In order to prevent flaws and attacks, we develop in this paper a framework allowing us analyse the underlying security protocols, taking into account the location of the agents and the transmission delay when exchanging messages. We propose two reduction results to render automatic verification possible relying on the existing verification tool ProVerif. Our first result allows one to consider a unique topology to catch all the possible attacks, and the second one simplifies the security analy- sis when considering terrorist frauds. Then, we perform a comprehensive case studies analysis (24 protocols to be precise) relying on our new framework. We obtain new proofs of security for some protocols and detect attacks on some others.

### Full version: [\[pdf\]](https://gitlab.inria.fr/adebant/db-verif/-/blob/master/full-version.pdf)
*(\* Note: the full version contains all the omitted proofs.
  \*)*

## ProVerif
The security analyses presented in this document have been conducted using the ProVerif tool.
Information about its installation and usage can be found on the
[ProVerif website](https://prosecco.gforge.inria.fr/personal/bblanche/proverif/).

However, the verification of the distance hijacking resistance requires a slightly modified version of the ProVerif tool.
It can be obtained executing the "./build" command in the provided
[proverif2.02pl1-modified/](https://gitlab.inria.fr/adebant/db-verif/-/tree/master/proverif2.02pl1-modified)" folder
(please refer to the [ProVerif website](https://prosecco.gforge.inria.fr/personal/bblanche/proverif/) for
further details about the compilation).

The two following executables are produced:
- proverif: standard version of the tool (used to check mafia and terrorist fraud resistance);
- proverif_dhr: modified version of the tool (used to check distance hijacking resistance).
Therefore, please note that "DHR_XXXXX.pv" files need to be run with the modified version of ProVerif by launching the following command:
	./proverif_dhr DHR_XXXXX.pv


## Case studies

The ProVerif files in this folder are used to analyse the security properties of different distance-bounding protocols.
For each protocol, we consider several security properties:
- [Distance Hijacking Resistance](https://gitlab.inria.fr/adebant/db-verif/-/tree/master/distance%20Hijacking), in the file "DHR_XXXXX.pv",
- [Mafia Fraud Resistance](https://gitlab.inria.fr/adebant/db-verif/-/tree/master/mafia%20Fraud), in the file "MFR_XXXXX.pv",
- [Terrorist Fraud Resistance](https://gitlab.inria.fr/adebant/db-verif/-/tree/master/terrorist%20Fraud), in the file "TFR_XXXXX.pv".

### Results are as follows:   
(Note: ❌ stands for an attack, ✅ stands for the absence of attack, *o.o.s.* refers to an out of scope and the corresponding file does not appear in the folder.)

| Protocol | Mafia fraud | Distance hijacking | Terrorist fraud |
| -------- | :---------: | :----------------: | :-------------: |
| Basin's Toy Example | ✅ | ✅ | ✅ |
| Brands and Chaum | ✅ | ❌ | *o.o.s.* |
| CRCS |  |  |  |
| - NoRevealing Sign | ✅ | ✅ | ❌ |
| - Revealing Sign | ✅ | ❌ | ❌ |
| Eff-PKB |  |  |  |
| - NoProtection | ✅ | ✅ | ✅ |
| - Protected | ✅ | ✅ | ✅ |
| Fiat-Shamir  | ✅ | ❌ | ❌ |
| Hancke and Kuhn | ✅ | ❌ | ❌ |
| MAD (one way) | ✅ | ❌ | *o.o.s.* |
| MasterCard-RRP | ✅ | ❌ | ❌ |
| Meadows |  |  |  |
| F = (nV,idP,nP) | ✅ | ✅ | ❌ |
| F = nV &oplus; H(idP,nP) | ✅ | ✅ | ❌ |
| F = (nV&oplus;nP,idP) | ✅ | ✅ | ❌ |
| F = (nV,nV&oplus;idP)  | ✅ | ❌ | ❌ |
| Munilla | ✅ | ✅ | ❌ |
| NXP | ✅ | ❌ | ❌ |
| PaySafe | ✅ | ❌ | ❌ |
| SKI | ✅ | ✅ | ✅ |
| SPADE |  |  |  |
| - Original | ❌ | ❌ | ✅ |
| - Fixed | ✅ | ❌ | ✅ |
| Swiss-Knife | ✅ | ✅ | ✅ |
| Swiss-Knife (modified) | ✅ | ✅ | ❌ |
| TREAD Asymmetric |  |  |  |
| - original (using id_priv) | ❌ | ❌ | ✅ |
| - fixed (using id_priv) | ✅ | ❌ | ✅ |
| - original (using id_pub) | ❌ | ❌ | ✅ |
| - fixed (using id_pub) | ✅ | ❌ | ✅ |
| TREAD Symmetric | ✅ | ❌ | ✅ |
