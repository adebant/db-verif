(* ----------------------------------------- *)
(*          MEADOWS (XORH) PROTOCOL          *)
(*      F(nV,nP,P) = XOR(nV,H(idP,nP))       *)
(*         Terrorist Fraud Analysis          *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a key K for MAC purposes and a one-way function H.
V -> P : nV
P -> V : Answer(nV,nP,idP) = XOR(nV,H(idP,nP))
P -> V : <idP,nP,nV> , MAC(K,<idP,nP,nV>)
*)

(* -- Signature --- *)
const OK:bitstring.

(* MAC *)
fun MAC(bitstring,bitstring):bitstring.

(* One-way function *)
fun H(bitstring):bitstring.

(* Shared key *)
fun sharedKey(bitstring,bitstring):bitstring [private].

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	phase 1;
	out(cP,nV);
	in(cP,inV1:bitstring);
	phase 2;
	let ipHash = cancelXOR1(inV1,nV) in
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	event EndV(vID,ipID).	
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.

let Verifier_P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.
	
let Verifier_P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 2;
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.	
	
let Verifier_P0P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	phase 1;
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.	
	
let Verifier_P0P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	phase 2;
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.

let Verifier_P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;
	in(cP,inV1:bitstring);
	let ipHash = cancelXOR1(inV1,nV) in
	phase 2;
	in(cP,(inV2:bitstring,inV3:bitstring));
	let (=ipID,ipN:bitstring,=nV) = inV2 in
	let TestNonce = equals(ipHash,H((ipID,ipN))) in
	let TestMAC = equals(inV3,MAC((ipID,ipN,nV),sharedKey(vID,ipID))) in
	0.	
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = XOR(inP1,H((pID,nP))) in
	out(cP,Ans);
	let pMAC = MAC((pID,nP,inP1),sharedKey(ivID,pID)) in
	let Conf = ((pID,nP,inP1),pMAC) in
	out(cP,Conf).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = XOR(inP1,H((pID,nP))) in
	out(cP,Ans);
	let pMAC = MAC((pID,nP,inP1),sharedKey(ivID,pID)) in
	let Conf = ((pID,nP,inP1),pMAC) in
	out(cP,Conf).

let Prover_P2(pID:bitstring,ivID:bitstring) =
	phase 2;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	let Ans = XOR(inP1,H((pID,nP))) in
	out(cP,Ans);
	let pMAC = MAC((pID,nP,inP1),sharedKey(ivID,pID)) in
	let Conf = ((pID,nP,inP1),pMAC) in
	out(cP,Conf).
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Prover Oracle --- *)
(*
let Prover_Oracle(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,(pID,nP));
	let pMAC = MAC((pID,nP,inP1),sharedKey(ivID,pID)) in
	let Conf = ((pID,nP,inP1),pMAC) in
	out(cP,Conf).
*)

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,sharedKey(idV0,idVC));
	out(cP,sharedKey(idV0,idPC));
	out(cP,sharedKey(idP0,idVC));
	out(cP,sharedKey(idP0,idPC));
	out(cP,sharedKey(idVC,idV0));	
	out(cP,sharedKey(idVC,idP0));
	out(cP,sharedKey(idVC,idVC));
	out(cP,sharedKey(idVC,idPC));
	out(cP,sharedKey(idPC,idV0));	
	out(cP,sharedKey(idPC,idP0));
	out(cP,sharedKey(idPC,idVC));
	out(cP,sharedKey(idPC,idPC));	
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	new NVone:bitstring;
	new NPone:bitstring;	
	out(cP,NVone);
	out(cP,(idP0,NPone));
	let pMACone = MAC((idP0,NPone,NVone),sharedKey(idV0,idP0)) in
	out(cP,((idP0,NPone,NVone),pMACone));	
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the TFR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P1(idV0,idV0) | !Verifier_P0P2(idV0,idV0) | !Verifier_P1P2(idV0,idV0) |	
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P1(idV0,idP0) | !Verifier_P0P2(idV0,idP0) | !Verifier_P1P2(idV0,idP0) |		
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P1(idV0,idVC) | !Verifier_P0P2(idV0,idVC) | !Verifier_P1P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P1(idV0,idPC) | !Verifier_P0P2(idV0,idPC) | !Verifier_P1P2(idV0,idPC) |
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |	
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |	
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P2(idP0,idP0) |	
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P2(idP0,idPC) |
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | 
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | 
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) 