(* ----------------------------------------- *)
(*               SKI PROTOCOL                *)
(*         Terrorist Fraud Analysis          *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a key K. We have a one-way function H, a linear function L.
P -> V : nP
V -> P : < XOR((a1,a2),H(nP,nV,mu)),mu,nV >
V -> P : cV
P -> V : Answer(cV,a1,a2,XOR(L(x,mu),XOR(a1,a2)))
*)

(* -- Signature --- *)
const OK:bitstring.

(* One-way function *)
fun H(bitstring,bitstring):bitstring.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Linear function *)
fun L(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; Linv(L(x,y),y) = x.

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Shared key *)
fun sharedKey(bitstring,bitstring):bitstring [private].

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring,bitstring):bitstring.

(* --- Constants / Channels --- *)
free cP:channel.
free cS:bitstring.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;	
	phase 1;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.
	
let Verifier_P1(vID:bitstring,ipID:bitstring) =
	phase 1;
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.

let Verifier_P2(vID:bitstring,ipID:bitstring) =
	phase 2;
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.	
	
let Verifier_P0P1(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.	
	
let Verifier_P0P2(vID:bitstring,ipID:bitstring) =
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.	
	
let Verifier_P1P2(vID:bitstring,ipID:bitstring) =	
	phase 1;
	in(cP,inV1:bitstring);
	new a1:bitstring;
	new a2:bitstring;
	new muV:bitstring;
	new nV:bitstring;
	let MV = XOR((a1,a2),H((inV1,nV,muV),sharedKey(vID,ipID))) in
	out(cP,(MV,muV,nV));
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	let TestAnswer = equals(Answer(cV,a1,a2,XOR(L(sharedKey(vID,ipID),muV),XOR(a1,a2))),inV2) in
	0.	
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	out(cP,nP);
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	out(cP,nP);
	phase 1;	
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).

let Prover_P2(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	out(cP,nP);
	phase 2;	
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).

let Prover_P0P1(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	out(cP,nP);
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).
	
let Prover_P0P2(pID:bitstring,ivID:bitstring) =	
	new nP:bitstring;
	out(cP,nP);
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).
	
let Prover_P1P2(pID:bitstring,ivID:bitstring) =	
	new nP:bitstring;
	out(cP,nP);
	phase 1;	
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Prover Oracle --- *)
(*
let Prover_Oracle(pID:bitstring,ivID:bitstring) =
	new nP:bitstring;
	out(cP,nP);
	in(cP,inP1:bitstring);
	let (inMV:bitstring,inMU:bitstring,inNV:bitstring) = inP1 in
	let (inA1:bitstring,inA2:bitstring) = cancelXOR2(inMV,H((nP,inNV,inMU),sharedKey(ivID,pID))) in
	out(cP,(inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))));
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,inA1,inA2,XOR(L(sharedKey(ivID,pID),inMU),XOR(inA1,inA2))) in
	out(cP,Ans).
*)

(* --- Global Process --- *)
process
	(* Corrupted agents near idV0 and idP0 *)
	new idVC:bitstring;
	new idPC:bitstring;
	(* Their secrets are known to the adversary *)
	out(cP,sharedKey(idV0,idVC));
	out(cP,sharedKey(idV0,idPC));
	out(cP,sharedKey(idP0,idVC));
	out(cP,sharedKey(idP0,idPC));
	out(cP,sharedKey(idVC,idV0));	
	out(cP,sharedKey(idVC,idP0));
	out(cP,sharedKey(idVC,idVC));
	out(cP,sharedKey(idVC,idPC));
	out(cP,sharedKey(idPC,idV0));	
	out(cP,sharedKey(idPC,idP0));
	out(cP,sharedKey(idPC,idVC));
	out(cP,sharedKey(idPC,idPC));	
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	new nPone:bitstring;
	out(cP,nPone);
	new a1one:bitstring;
	new a2one:bitstring;
	new muVone:bitstring;
	new nVone:bitstring;
	let MVone = XOR((a1one,a2one),H((nPone,nVone,muVone),sharedKey(idV0,idP0))) in
	out(cP,(MVone,muVone,nVone));
	new cVone:bitstring;
	out(cP,cVone);
	out(cP,(a1one,a2one,XOR(L(sharedKey(idV0,idP0),muVone),XOR(a1one,a2one))));
	let Ansone = Answer(cVone,a1one,a2one,XOR(L(sharedKey(idV0,idP0),muVone),XOR(a1one,a2one))) in
	out(cP,Ansone);
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the TFR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* V0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P1(idV0,idV0) | !Verifier_P0P2(idV0,idV0) | !Verifier_P1P2(idV0,idV0) |	
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P1(idV0,idP0) | !Verifier_P0P2(idV0,idP0) | !Verifier_P1P2(idV0,idP0) |		
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P1(idV0,idVC) | !Verifier_P0P2(idV0,idVC) | !Verifier_P1P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P1(idV0,idPC) | !Verifier_P0P2(idV0,idPC) | !Verifier_P1P2(idV0,idPC) |
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P1(idV0,idV0) | !Prover_P0P2(idV0,idV0) | !Prover_P1P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P1(idV0,idP0) | !Prover_P0P2(idV0,idP0) | !Prover_P1P2(idV0,idP0) |		
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0P1(idV0,idVC) | !Prover_P0P2(idV0,idVC) | !Prover_P1P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	!Prover_P0P1(idV0,idPC) | !Prover_P0P2(idV0,idPC) | !Prover_P1P2(idV0,idPC) |	
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P2(idP0,idP0) |	
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P2(idP0,idPC) |
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | !Prover_P0P2(idP0,idV0) |
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | !Prover_P0P2(idP0,idP0) |	
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) | !Prover_P0P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) | !Prover_P0P2(idP0,idPC)