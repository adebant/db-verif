(* ----------------------------------------- *)
(*           Fiat-Shamir Protocol            *)
(*           Mafia Fraud Analysis            *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* 
Verifier and Prover shares a same number n, and Verifier knows (skP)^2 mod n for each prover.
P -> V : SQR(rP),commit(nP)
V -> P : cV
P -> V : nP
P -> V : MULT(POW(skP,(cV,nP)),rP)
where s = H(g,pkP,pkV,pkV^skP,nP) = H(g,pkP,pkV,pkP^skV,nP) a common secret.
*)

(* -- Signature --- *)
const OK:bitstring.

(* Secret Exponent *)
fun secKey(bitstring):bitstring [private].

(* Multiplication *)
fun MULT(bitstring,bitstring):bitstring.
equation forall x:bitstring, y:bitstring; MULT(x,y) = MULT(y,x).

(* Group Exponentiation *)
fun POW(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring, z:bitstring; POWPOW(POW(x,y),z) = POW(x,MULT(y,z)).

(* Square function *)
fun SQR(bitstring):bitstring.

fun Square(bitstring):bitstring
reduc forall x:bitstring, y:bitstring, z:bitstring, t:bitstring; Square(MULT(POW(x,y),POW(z,t))) = MULT(POW(SQR(x),y),POW(SQR(z),t))
	otherwise forall x:bitstring, y:bitstring, z:bitstring; Square(MULT(POW(x,y),z)) = MULT(POW(SQR(x),y),SQR(z))
	otherwise forall x:bitstring, y:bitstring, z:bitstring; Square(MULT(z,POW(x,y))) = MULT(SQR(z),POW(SQR(x),y))
	otherwise forall x:bitstring, y:bitstring, z:bitstring; Square(POW(MULT(x,y),z)) = POW(MULT(SQR(x),SQR(y)),z)
	otherwise forall x:bitstring, y:bitstring; Square(MULT(x,y)) = MULT(SQR(x),SQR(y))
	otherwise forall x:bitstring, y:bitstring; Square(POW(x,y)) = POW(SQR(x),y)
	otherwise forall x:bitstring; Square(x) = SQR(x).

(* Commit *)
fun Commit(bitstring):bitstring.

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* --- Constants / Channels --- *)
free cP:channel.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	phase 1;
	out(cP,cV);
	in(cP,inV3:bitstring);
	phase 2;
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.
	
let Verifier_P1(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	phase 1;	
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.	
	
let Verifier_P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	phase 2;	
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.	

let Verifier_P0P0P1(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	phase 1;
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.

let Verifier_P0P0P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV3:bitstring);
	phase 2;
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.		
	
let Verifier_P0P1P1(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	phase 1;	
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.	

let Verifier_P0P1P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	phase 1;	
	in(cP,inV3:bitstring);
	phase 2;
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.	

let Verifier_P0P2P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);
	phase 2;	
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.		
	
let Verifier_P1P1P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	phase 1;
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);	
	in(cP,inV3:bitstring);
	phase 2;
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.		
	
let Verifier_P1P2P2(vID:bitstring,ipID:bitstring) =
	let ipkP = SQR(secKey(ipID)) in
	phase 1;
	in(cP,(inV1:bitstring,inV2:bitstring));
	new cV:bitstring;
	out(cP,cV);	
	phase 2;	
	in(cP,inV3:bitstring);
	in(cP,inV4:bitstring);
	let checkComm = equals(inV2,Commit(inV3)) in
	let comp = MULT(POW(ipkP,(cV,inV3)),inV1) in
	let checkAns = equals(Square(inV4),comp) in
	0.		
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	let pkP = SQR(secKey(pID)) in
	new R:bitstring;
	new B:bitstring;
	let commB = Commit(B) in
	let sqrR = SQR(R) in
	out(cP,(sqrR,commB));
	in(cP,inP1:bitstring);
	out(cP,B);
	let comp = MULT(POW(secKey(pID),(inP1,B)),R) in
	out(cP,comp).
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	let pkP = SQR(secKey(pID)) in
	new R:bitstring;
	new B:bitstring;
	let commB = Commit(B) in
	let sqrR = SQR(R) in
	out(cP,(sqrR,commB));
	phase 1;
	in(cP,inP1:bitstring);
	out(cP,B);
	let comp = MULT(POW(secKey(pID),(inP1,B)),R) in
	out(cP,comp).
	
let Prover_P2(pID:bitstring,ivID:bitstring) =
	let pkP = SQR(secKey(pID)) in
	new R:bitstring;
	new B:bitstring;
	let commB = Commit(B) in
	let sqrR = SQR(R) in
	out(cP,(sqrR,commB));
	phase 2;
	in(cP,inP1:bitstring);
	out(cP,B);
	let comp = MULT(POW(secKey(pID),(inP1,B)),R) in
	out(cP,comp).	
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Prover Oracle --- *)
(*
let Prover_Oracle(pID:bitstring,ivID:bitstring) =
	let pkP = SQR(secKey(pID)) in
	new R:bitstring;
	new B:bitstring;
	let commB = Commit(B) in
	let sqrR = SQR(R) in
	out(cP,(sqrR,commB));
	out(cP,B);
	in(cP,inP1:bitstring);
	out(cP,B);
	let comp = MULT(POW(secKey(pID),(inP1,B)),R) in
	out(cP,comp).
*)

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,secKey(idVC));
	out(cP,secKey(idPC));
	out(cP,SQR(secKey(idV0)));
	out(cP,SQR(secKey(idP0)));
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	new Rone:bitstring;
	new Bone:bitstring;
	out(cP,(SQR(Rone),Commit(Bone)));
	out(cP,Bone);
	new CVone:bitstring;
	out(cP,CVone);
	out(cP,Bone);
	let ANSone = MULT(POW(secKey(idP0),(CVone,Bone)),Rone) in
	out(cP,ANSone);
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the MFR property *)
	Verifier_Test(idV0,idP0) |
	(* -- Processes that can also speak during the execution : -- *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P0P1(idV0,idVC) | !Verifier_P0P0P2(idV0,idVC) | !Verifier_P0P1P1(idV0,idVC) |	
	!Verifier_P0P1P2(idV0,idVC) | !Verifier_P0P2P2(idV0,idVC) | 
	!Verifier_P1P1P2(idV0,idVC) | !Verifier_P1P2P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P0P1(idV0,idPC) | !Verifier_P0P0P2(idV0,idPC) | !Verifier_P0P1P1(idV0,idPC) |	
	!Verifier_P0P1P2(idV0,idPC) | !Verifier_P0P2P2(idV0,idPC) | 
	!Verifier_P1P1P2(idV0,idPC) | !Verifier_P1P2P2(idV0,idPC) |	
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |	
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |	
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P0P2(idP0,idV0) | !Verifier_P0P2P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P0P2(idP0,idP0) | !Verifier_P0P2P2(idP0,idP0) |
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P0P2(idP0,idVC) | !Verifier_P0P2P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P0P2(idP0,idPC) | !Verifier_P0P2P2(idP0,idPC) |	
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | 
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | 
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) 