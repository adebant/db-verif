(* ----------------------------------------- *)
(*          MASTERCARD-RRP PROTOCOL          *)
(*          Terrorist Fraud Analysis         *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* 
R and C exchange public constants 
--- Begin Rapid Phase ---
R -> C : PConstant, Amount, rNonce 
C -> R : cNonce, PConstants
--- End Rapid Phase ---
C provides info to authenticate itself to R
*)

(* -- Signature --- *)
const OK:bitstring.
	
(* Public Key Signature *)
fun pubKey(bitstring):bitstring.
fun secKey(bitstring):bitstring [private].
fun Sign(bitstring,bitstring):bitstring.
fun Verify(bitstring,bitstring):bitstring
reduc forall m:bitstring, k:bitstring; Verify(Sign(m,secKey(k)),pubKey(k)) = m.
	
(* Mac *)
fun genKey(bitstring,bitstring):bitstring.
fun Mac(bitstring,bitstring):bitstring.
	
(* Equality test *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Card function *)
fun Atc(bitstring):bitstring [private].
fun TestATC(bitstring):bitstring
reduc forall id:bitstring; TestATC(Atc(id)) = id.
fun timingInfo(bitstring):bitstring.

(* --- Constants / Channels --- *)
free cP:channel.
free idR0:bitstring. (* Honest Player acting as Test Reader *)
free idC0:bitstring. (* Honest Player far-away from idR0 *)
free idRC:bitstring. (* Dishonest Player close to idR0 *)
free idCC:bitstring. (* Dishonest Player close to idC0 *)
free idBank:bitstring.

free AFL:bitstring.
free AIP:bitstring.
free EXCHANGE_RELAY_RESISTANCE_DATA:bitstring.
free GENERATE_AC:bitstring.
free GET_PROCESSING_OPTIONS:bitstring.
free PDOL:bitstring.
free READ_RECORD:bitstring.
free SELECT_PAYPASS:bitstring.
free SELECTED_PAYPASS:bitstring.

(* --- Events --- *)
event EndR(bitstring,bitstring,bitstring).

(* NB: We removed the 2 initials in/out since they are not really relevant for the security of the protocol. *)

(* --- Processes --- *)
let Reader_Test(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	(* We output the amount to give it to the attacker since it's not a secret *)	
	out(cP,rAmount);
(*	out(cP,SELECT_PAYPASS);				*)        
(*	in(cP,=SELECTED_PAYPASS);			*)             
(*	out(cP,GET_PROCESSING_OPTIONS);		*)             
(*	in(cP,(=AIP,=AFL));					*)					   
	new rNonce:bitstring;
	phase 1;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	phase 2;
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	event EndR(rID,cID,cTimingInfo).
	
let Reader_P0(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.
	
let Reader_P1(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	phase 1;
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.

let Reader_P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	phase 2;
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.

let Reader_P0P0P1(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	phase 1;
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.	
	
let Reader_P0P0P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	phase 2;
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.		
	
let Reader_P0P1P1(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	phase 1;
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.

let Reader_P0P1P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	phase 1;
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	phase 2;
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.	

let Reader_P0P2P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	phase 2;
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.	

let Reader_P1P1P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	phase 1;
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));
	phase 2;	
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.	

let Reader_P1P2P2(rID:bitstring,icID:bitstring) =
	new rAmount:bitstring;
	out(cP,rAmount);				   
	new rNonce:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonce));
	phase 1;
	in(cP,(cNonce:bitstring,cTimingInfo:bitstring));
	out(cP,READ_RECORD);
	phase 2;	
	in(cP, cCert:bitstring);
	out(cP,(GENERATE_AC,rNonce,rAmount));	
	in(cP,(cSDAD:bitstring,cAC:bitstring));
	let (cID:bitstring,cPubK:bitstring) = Verify(cCert,pubKey(idBank)) in	
	let TestID = equals(cID,icID) in
	let (=rNonce,=cNonce,=rAmount,=cTimingInfo,=cAC) = Verify(cSDAD,cPubK) in
	0.		
	
let Card_P0(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
(*	in(cP,=SELECT_PAYPASS);				*)
(*	out(cP,SELECTED_PAYPASS);			*)
(*	in(cP,=GET_PROCESSING_OPTIONS);		*)
(*	out(cP,(AIP,AFL));					*)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).
	
let Card_P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).

let Card_P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 2;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).

let Card_P0P0P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	phase 1;
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).	
	
let Card_P0P0P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	phase 2;
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).
	
let Card_P0P1P1(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	phase 1;
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).
	
let Card_P0P1P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	phase 1;
	in(cP,=READ_RECORD);
	out(cP,Cert);
	phase 2;
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).
	
let Card_P0P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	phase 2;
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).

let Card_P1P1P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	in(cP,=READ_RECORD);
	out(cP,Cert);
	phase 2;
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).	
	
let Card_P1P2P2(cID:bitstring,irID:bitstring) =
	(* Card initialization  *)
    let TimingInfo = timingInfo(cID) in
	in(cP, Cert:bitstring);
	let TestCert = equals((cID,pubKey(cID)), Verify(Cert,pubKey(idBank))) in
	in(cP, ATC:bitstring);
	let TestATC = equals(cID, TestATC(ATC)) in
	(* --- Start of Card real process --- *)
	new cNonce:bitstring;
	phase 1;
	in(cP,(=EXCHANGE_RELAY_RESISTANCE_DATA,rNonce:bitstring));
	out(cP,(cNonce,TimingInfo));
	phase 2;
	in(cP,=READ_RECORD);
	out(cP,Cert);
	in(cP,(=GENERATE_AC,=rNonce,rAmount:bitstring));
	let macKey = genKey(ATC,secKey(cID)) in
	let AC = Mac((ATC,rAmount,rNonce),macKey) in
	let SDAD = Sign((rNonce,cNonce,rAmount,TimingInfo,AC),secKey(cID)) in
	out(cP,(SDAD,AC)).	

(* --- Security Property --- *)
query x:bitstring; event(EndR(idR0,idC0,x)).
query x:bitstring,y:bitstring,z:bitstring; event(EndR(x,y,z)) ==> z = timingInfo(y).

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,(idCC,secKey(idCC)));
	out(cP,(idRC,secKey(idRC)));
	(* Outputting Cards Informations to initiate them *)
	out(cP,(Atc(idC0),timingInfo(idC0),Sign((idC0,pubKey(idC0)),secKey(idBank))));
	out(cP,(Atc(idR0),timingInfo(idR0),Sign((idR0,pubKey(idR0)),secKey(idBank))));
	out(cP,(Atc(idCC),timingInfo(idCC),Sign((idCC,pubKey(idCC)),secKey(idBank))));
	out(cP,(Atc(idRC),timingInfo(idRC),Sign((idRC,pubKey(idRC)),secKey(idBank))));
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	(* we suppose the card has been initialized using the idC0's informations *)
	new rNonceone:bitstring;
	new cNonceone:bitstring;
	new Amountone:bitstring;
	out(cP,(EXCHANGE_RELAY_RESISTANCE_DATA,rNonceone));
	out(cP, READ_RECORD);
	out(cP, Sign((idC0,pubKey(idC0)),secKey(idBank)));
	out(cP, (GENERATE_AC, rNonceone, Amountone));
	let macKeyone = genKey(Atc(idC0),secKey(idC0)) in
	let ACone = Mac((Atc(idC0),Amountone,rNonceone),macKeyone) in
	let SDADone = Sign((rNonceone,cNonceone,Amountone,timingInfo(idC0),ACone),secKey(idC0)) in
	out(cP,(SDADone,ACone));
	(* Now we let the adversary (almost) alone *)
	(* Honest Reader used to test the MFR property *)
	Reader_Test(idR0,idC0) | 
	(* Processes that can also speak during the execution : *)	
	(* idR0 may execute Card/Reader roles at anytime *)	
	!Reader_P0(idR0,idR0) | !Reader_P1(idR0,idR0) | !Reader_P2(idR0,idR0) |
	!Reader_P0P0P1(idR0,idR0) | !Reader_P0P0P2(idR0,idR0) | !Reader_P0P1P1(idR0,idR0) |
	!Reader_P0P1P2(idR0,idR0) | !Reader_P0P2P2(idR0,idR0) | !Reader_P1P1P2(idR0,idR0) | !Reader_P1P2P2(idR0,idR0) | 
	!Card_P0(idR0,idR0) | !Card_P1(idR0,idR0) | !Card_P2(idR0,idR0) |
	!Card_P0P0P1(idR0,idR0) | !Card_P0P0P2(idR0,idR0) | !Card_P0P1P1(idR0,idR0) |
	!Card_P0P1P2(idR0,idR0) | !Card_P0P2P2(idR0,idR0) | !Card_P1P1P2(idR0,idR0) | !Card_P1P2P2(idR0,idR0) |
	
	!Reader_P0(idR0,idC0) | !Reader_P1(idR0,idC0) | !Reader_P2(idR0,idC0) |
	!Reader_P0P0P1(idR0,idC0) | !Reader_P0P0P2(idR0,idC0) | !Reader_P0P1P1(idR0,idC0) |
	!Reader_P0P1P2(idR0,idC0) | !Reader_P0P2P2(idR0,idC0) | !Reader_P1P1P2(idR0,idC0) | !Reader_P1P2P2(idR0,idC0) | 
	!Card_P0(idR0,idC0) | !Card_P1(idR0,idC0) | !Card_P2(idR0,idC0) |
	!Card_P0P0P1(idR0,idC0) | !Card_P0P0P2(idR0,idC0) | !Card_P0P1P1(idR0,idC0) |
	!Card_P0P1P2(idR0,idC0) | !Card_P0P2P2(idR0,idC0) | !Card_P1P1P2(idR0,idC0) | !Card_P1P2P2(idR0,idC0) |	
	
	!Reader_P0(idR0,idCC) | !Reader_P1(idR0,idCC) | !Reader_P2(idR0,idCC) |
	!Reader_P0P0P1(idR0,idCC) | !Reader_P0P0P2(idR0,idCC) | !Reader_P0P1P1(idR0,idCC) |
	!Reader_P0P1P2(idR0,idCC) | !Reader_P0P2P2(idR0,idCC) | !Reader_P1P1P2(idR0,idCC) | !Reader_P1P2P2(idR0,idCC) | 
	!Card_P0(idR0,idCC) | !Card_P1(idR0,idCC) | !Card_P2(idR0,idCC) |
	!Card_P0P0P1(idR0,idCC) | !Card_P0P0P2(idR0,idCC) | !Card_P0P1P1(idR0,idCC) |
	!Card_P0P1P2(idR0,idCC) | !Card_P0P2P2(idR0,idCC) | !Card_P1P1P2(idR0,idCC) | !Card_P1P2P2(idR0,idCC) |	
	
	!Reader_P0(idR0,idRC) | !Reader_P1(idR0,idRC) | !Reader_P2(idR0,idRC) |
	!Reader_P0P0P1(idR0,idRC) | !Reader_P0P0P2(idR0,idRC) | !Reader_P0P1P1(idR0,idRC) |
	!Reader_P0P1P2(idR0,idRC) | !Reader_P0P2P2(idR0,idRC) | !Reader_P1P1P2(idR0,idRC) | !Reader_P1P2P2(idR0,idRC) | 
	!Card_P0(idR0,idRC) | !Card_P1(idR0,idRC) | !Card_P2(idR0,idRC) |
	!Card_P0P0P1(idR0,idRC) | !Card_P0P0P2(idR0,idRC) | !Card_P0P1P1(idR0,idRC) |
	!Card_P0P1P2(idR0,idRC) | !Card_P0P2P2(idR0,idRC) | !Card_P1P1P2(idR0,idRC) | !Card_P1P2P2(idR0,idRC) |	

	(* idC0 only speaks in P0/P2 *)
	!Reader_P0(idC0,idR0) | !Reader_P2(idC0,idR0) |  !Reader_P0P0P2(idC0,idR0) | !Reader_P0P2P2(idC0,idR0) | 
	!Card_P0(idC0,idR0) | !Card_P0(idC0,idR0) | !Card_P0P0P2(idC0,idR0) | !Card_P0P2P2(idC0,idR0) |
    
    !Reader_P0(idC0,idC0) | !Reader_P2(idC0,idC0) | !Reader_P0P0P2(idC0,idC0) | !Reader_P0P2P2(idC0,idC0) | 
	!Card_P0(idC0,idC0) | !Card_P0(idC0,idC0) | !Card_P0P0P2(idC0,idC0) | !Card_P0P2P2(idC0,idC0) | 
    
    !Reader_P0(idC0,idCC) | !Reader_P2(idC0,idCC) | !Reader_P0P0P2(idC0,idCC) | !Reader_P0P2P2(idC0,idCC) | 
	!Card_P0(idC0,idCC) | !Card_P0(idC0,idCC) | !Card_P0P0P2(idC0,idCC) | !Card_P0P2P2(idC0,idCC) | 
    
    !Reader_P0(idC0,idRC) | !Reader_P2(idC0,idRC) | !Reader_P0P0P2(idC0,idRC) | !Reader_P0P2P2(idC0,idRC) | 
	!Card_P0(idC0,idRC) | !Card_P0(idC0,idRC) |	!Card_P0P0P2(idC0,idRC) | !Card_P0P2P2(idC0,idRC)
