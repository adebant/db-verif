(* ----------------------------------------- *)
(*           SWISS KNIFE PROTOCOL            *)
(*         Terrorist Fraud Analysis          *)
(* ----------------------------------------- *)

(* -- Protocol Scheme -- *)
(* P,V share a key K and a one-way keyed-function H, there is a system-wide constant cS.
V -> P : nV
P -> V : nP
V -> P : cV
P -> V : Answer(cV,H(<cS,nP>,K),XOR(R0,K))
P -> V : H(<idP,nV,nP,challenges>,K)
V -> P : H(nP,K)
*)

(* -- Signature --- *)
const OK:bitstring.

(* One-Way keyed-function *)
fun H(bitstring,bitstring):bitstring.

(* Weak XOR *)
fun XOR(bitstring,bitstring):bitstring.
reduc forall x:bitstring, y:bitstring; commutXOR(XOR(x,y)) = XOR(y,x).
reduc forall x:bitstring, y:bitstring; cancelXOR1(XOR(x,y),x) = y.
reduc forall x:bitstring, y:bitstring; cancelXOR2(XOR(x,y),y) = x.
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR1(y,XOR(x,XOR(y,z))) = XOR(x,z).
reduc forall x:bitstring, y:bitstring, z:bitstring; cancelTXOR2(z,XOR(x,XOR(y,z))) = XOR(x,y).

(* Equality Check Function *)
fun equals(bitstring,bitstring):bitstring
reduc forall x:bitstring; equals(x,x) = OK.

(* Shared key *)
fun sharedKey(bitstring,bitstring):bitstring [private].

(* Answering function *)
fun Answer(bitstring,bitstring,bitstring):bitstring.

(* --- Constants / Channels --- *)
free cP:channel.
free cS:bitstring.
free idV0:bitstring. (* Honest Player acting as Test Verifier *)
free idP0:bitstring. (* Honest Player far-away from idV0 *)
free idVC:bitstring. (* Dishonest Player close to idV0 *)
free idPC:bitstring. (* Dishonest Player close to idP0 *)

(* --- Events --- *)
event EndV(bitstring,bitstring).

(* --- Processes --- *)
let Verifier_Test(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in
	new cV:bitstring;	
	phase 1;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf);
	event EndV(vID,ipID).
	
let Verifier_P0(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	
	
let Verifier_P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).

let Verifier_P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 2;	
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).	
	
let Verifier_P0P0P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 1;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).

let Verifier_P0P0P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P0P1P1(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P0P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 1;
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P0P2P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P1P1P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	in(cP,inV2:bitstring);
	phase 2;
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Verifier_P1P2P2(vID:bitstring,ipID:bitstring) =
	new nV:bitstring;
	out(cP,nV);
	phase 1;	
	in(cP,inV1:bitstring);	
	let RV0 = H((cS,inV1),sharedKey(vID,ipID)) in
	let RV1 = XOR(RV0,sharedKey(vID,ipID)) in	
	new cV:bitstring;
	out(cP,cV);
	phase 2;
	in(cP,inV2:bitstring);
	in(cP,inV3:bitstring);
	let TestTranscript = equals(H((cV,ipID,nV,inV1),sharedKey(vID,ipID)),inV3) in
	let TestAnswer = equals(Answer(cV,RV0,RV1),inV2) in
	let Conf = H(inV1,sharedKey(vID,ipID)) in
	out(cP,Conf).
	
let Prover_P0(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P1(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P2(pID:bitstring,ivID:bitstring) =
	phase 2;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P0P1(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 1;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P0P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P1P1(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P1P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 1;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P0P2P2(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.	
	
let Prover_P1P1P2(pID:bitstring,ivID:bitstring) =
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	phase 2;
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
let Prover_P1P2P2(pID:bitstring,ivID:bitstring) =	
	phase 1;
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	phase 2;
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
	
(* --- Security Property --- *)
query event(EndV(idV0,idP0)).

(* --- Prover Oracle --- *)
(*
let Prover_Oracle(pID:bitstring,ivID:bitstring) =
	in(cP,inP1:bitstring);
	new nP:bitstring;
	out(cP,nP);
	let RP0 = H((cS,nP),sharedKey(ivID,pID)) in
	let RP1 = XOR(RP0,sharedKey(ivID,pID)) in
	out(cP,(RP0,RP1));
	in(cP,inP2:bitstring);
	let Ans = Answer(inP2,RP0,RP1) in
	out(cP,Ans);
	let Trans = H((inP2,pID,inP1,nP),sharedKey(ivID,pID)) in
	out(cP,Trans);
	in(cP,inP3:bitstring);
	let TestConf = equals(H(nP,sharedKey(ivID,pID)),inP3) in
	0.
*)

(* --- Global Process --- *)
process
	(* Secrets known to the adversary *)
	out(cP,sharedKey(idV0,idVC));
	out(cP,sharedKey(idV0,idPC));
	out(cP,sharedKey(idP0,idVC));
	out(cP,sharedKey(idP0,idPC));
	out(cP,sharedKey(idVC,idV0));	
	out(cP,sharedKey(idVC,idP0));
	out(cP,sharedKey(idVC,idVC));
	out(cP,sharedKey(idVC,idPC));
	out(cP,sharedKey(idPC,idV0));	
	out(cP,sharedKey(idPC,idP0));
	out(cP,sharedKey(idPC,idVC));
	out(cP,sharedKey(idPC,idPC));	
	(* We provide the adversary with the content of the frame obtained by running the Prover_Oracle with the Verifier *)
	new NVone:bitstring;
	new NPone:bitstring;
	new CVone:bitstring;	
	out(cP,NVone);
	out(cP,NPone);
	let RP0one = H((cS,NPone),sharedKey(idV0,idP0)) in
	let RP1one = XOR(RP0one,sharedKey(idV0,idP0)) in
	out(cP,(RP0one,RP1one));
	out(cP,CVone);
	let Ansone = Answer(CVone,RP0one,RP1one) in
	out(cP,Ansone);
	let Transone = H((CVone,idP0,NVone,NPone),sharedKey(idV0,idP0)) in
	out(cP,Transone);	
	let Confone = H(NPone,sharedKey(idV0,idP0)) in
	out(cP,Confone);
	(* Now we let the adversary (almost) alone *)
	(* Honest Verifier used to test the TFR property *)
	Verifier_Test(idV0,idP0) |
	(* Processes that can also speak during the execution : *)
	(* idV0 may execute Prover/Verifier roles at anytime *)
	!Verifier_P0(idV0,idV0) | !Verifier_P1(idV0,idV0) | !Verifier_P2(idV0,idV0) |
	!Verifier_P0P0P1(idV0,idV0) | !Verifier_P0P0P2(idV0,idV0) | !Verifier_P0P1P1(idV0,idV0) |	
	!Verifier_P0P1P2(idV0,idV0) | !Verifier_P0P2P2(idV0,idV0) | 
	!Verifier_P1P1P2(idV0,idV0) | !Verifier_P1P2P2(idV0,idV0) |
	!Verifier_P0(idV0,idP0) | !Verifier_P1(idV0,idP0) | !Verifier_P2(idV0,idP0) |
	!Verifier_P0P0P1(idV0,idP0) | !Verifier_P0P0P2(idV0,idP0) | !Verifier_P0P1P1(idV0,idP0) |	
	!Verifier_P0P1P2(idV0,idP0) | !Verifier_P0P2P2(idV0,idP0) | 
	!Verifier_P1P1P2(idV0,idP0) | !Verifier_P1P2P2(idV0,idP0) |
	!Verifier_P0(idV0,idVC) | !Verifier_P1(idV0,idVC) | !Verifier_P2(idV0,idVC) |
	!Verifier_P0P0P1(idV0,idVC) | !Verifier_P0P0P2(idV0,idVC) | !Verifier_P0P1P1(idV0,idVC) |	
	!Verifier_P0P1P2(idV0,idVC) | !Verifier_P0P2P2(idV0,idVC) | 
	!Verifier_P1P1P2(idV0,idVC) | !Verifier_P1P2P2(idV0,idVC) |
	!Verifier_P0(idV0,idPC) | !Verifier_P1(idV0,idPC) | !Verifier_P2(idV0,idPC) |
	!Verifier_P0P0P1(idV0,idPC) | !Verifier_P0P0P2(idV0,idPC) | !Verifier_P0P1P1(idV0,idPC) |	
	!Verifier_P0P1P2(idV0,idPC) | !Verifier_P0P2P2(idV0,idPC) | 
	!Verifier_P1P1P2(idV0,idPC) | !Verifier_P1P2P2(idV0,idPC) |	
	!Prover_P0(idV0,idV0) | !Prover_P1(idV0,idV0) | !Prover_P2(idV0,idV0) |
	!Prover_P0P0P1(idV0,idV0) | !Prover_P0P0P2(idV0,idV0) | !Prover_P0P1P1(idV0,idV0) |	
	!Prover_P0P1P2(idV0,idV0) | !Prover_P0P2P2(idV0,idV0) | 
	!Prover_P1P1P2(idV0,idV0) | !Prover_P1P2P2(idV0,idV0) |
	!Prover_P0(idV0,idP0) | !Prover_P1(idV0,idP0) | !Prover_P2(idV0,idP0) |
	!Prover_P0P0P1(idV0,idP0) | !Prover_P0P0P2(idV0,idP0) | !Prover_P0P1P1(idV0,idP0) |	
	!Prover_P0P1P2(idV0,idP0) | !Prover_P0P2P2(idV0,idP0) | 
	!Prover_P1P1P2(idV0,idP0) | !Prover_P1P2P2(idV0,idP0) |
	!Prover_P0(idV0,idVC) | !Prover_P1(idV0,idVC) | !Prover_P2(idV0,idVC) |
	!Prover_P0P0P1(idV0,idVC) | !Prover_P0P0P2(idV0,idVC) | !Prover_P0P1P1(idV0,idVC) |	
	!Prover_P0P1P2(idV0,idVC) | !Prover_P0P2P2(idV0,idVC) | 
	!Prover_P1P1P2(idV0,idVC) | !Prover_P1P2P2(idV0,idVC) |
	!Prover_P0(idV0,idPC) | !Prover_P1(idV0,idPC) | !Prover_P2(idV0,idPC) |
	!Prover_P0P0P1(idV0,idPC) | !Prover_P0P0P2(idV0,idPC) | !Prover_P0P1P1(idV0,idPC) |	
	!Prover_P0P1P2(idV0,idPC) | !Prover_P0P2P2(idV0,idPC) | 
	!Prover_P1P1P2(idV0,idPC) | !Prover_P1P2P2(idV0,idPC) |
	(* idP0 only speaks in P0/P2 *)
	!Verifier_P0(idP0,idV0) | !Verifier_P2(idP0,idV0) | !Verifier_P0P0P2(idP0,idV0) | !Verifier_P0P2P2(idP0,idV0) |
	!Verifier_P0(idP0,idP0) | !Verifier_P2(idP0,idP0) | !Verifier_P0P0P2(idP0,idP0) | !Verifier_P0P2P2(idP0,idP0) |
	!Verifier_P0(idP0,idVC) | !Verifier_P2(idP0,idVC) | !Verifier_P0P0P2(idP0,idVC) | !Verifier_P0P2P2(idP0,idVC) |
	!Verifier_P0(idP0,idPC) | !Verifier_P2(idP0,idPC) | !Verifier_P0P0P2(idP0,idPC) | !Verifier_P0P2P2(idP0,idPC) |	
	!Prover_P0(idP0,idV0) | !Prover_P2(idP0,idV0) | !Prover_P0P0P2(idP0,idV0) | !Prover_P0P2P2(idP0,idV0) |
	!Prover_P0(idP0,idP0) | !Prover_P2(idP0,idP0) | !Prover_P0P0P2(idP0,idP0) | !Prover_P0P2P2(idP0,idP0) |
	!Prover_P0(idP0,idVC) | !Prover_P2(idP0,idVC) | !Prover_P0P0P2(idP0,idVC) | !Prover_P0P2P2(idP0,idVC) |
	!Prover_P0(idP0,idPC) | !Prover_P2(idP0,idPC) | !Prover_P0P0P2(idP0,idPC) | !Prover_P0P2P2(idP0,idPC) 